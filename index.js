var express = require('express');
var Sequelize = require('sequelize');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');

var app = express();


const sequelize = new Sequelize('mysql://root:zetsu123@localhost:3306/expressjm');

var User = sequelize.define('User', {
    name:{
        type: Sequelize.STRING
    },
    lastname: Sequelize.STRING,
});

User.sync().then(()=>{
    return User.create({
        name:'Lorrane',
        lastname: 'Luna'
    })
})
app.set('views', './views');
app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(methodOverride((req,res)=>{
    if(req.body && typeof req.body == 'object' && '_method' in req.body){
        var method = req.body._method;

        delete req.body._method;
        return method;
    }
}))

var port = 3000;

app.get('/', (req,res)=>{
    console.log('index');
    res.render('index', {message:'batata'});
})

app.get('/users/create', (req, res)=>{
    res.render('new_users', {message: 'new user'});
})

app.post('/users/create', (req,res)=>{
    User.create(req.body).then(()=>{
        res.render('new_users', {message:'cadastrado com sucesso'})
    })
})

app.get('/users/edit/:id', (req,res)=>{
      User
        .findById(req.params.id).then((result)=>{
            res.render('edit_user', {message:'Editing User', data:result});
        })
})

app.put('/users/edit/:id', (req,res)=>{
    User.update(req.body, {where: {id:req.params.id}}).then((result)=>{
        res.redirect('/users');
    })
})

app.get('/users/:id', (req,res)=>{
    User
        .findById(req.params.id).then((result)=>{
            res.render('user', {message:'A user', data:result});
        })

   /* User
    .findOne({where:{id:req.params.id}}).then((result)=>{
        res.render('user', {message:'A User', data:result} )
    })*/
})




app.delete('/users/:id', (req,res)=>{
    User    
        .destroy({where:{id:req.params.id}}).then(()=>{
            res.redirect('/users')
        })
        .catch((err)=>{
            console.log('Error => ', err);
        })
})

app.get('/users', (req,res)=>{
    User.findAll().then((result)=>{
        res.render('users', {message:'list',data:result});
    }).catch((err)=>{
        console.log(err);
    })
})
app.listen(port, ()=>{
    console.log('escutando na porta ' + port);
})